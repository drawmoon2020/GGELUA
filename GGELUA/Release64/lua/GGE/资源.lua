--[[
    @Author       : baidwwy
    @Date         : 2021-02-11 11:49:09
    @LastEditTime : 2021-05-06 08:30:34
--]]
local GGE资源包
local _ENV = require("SDL")

local GGE资源 = class"GGE资源"

function GGE资源:初始化()
    self._path = {}
    self._pack = {}
end

function GGE资源:添加资源包(file,psd)
    self._pack[file] = GGE资源包(file,psd)
    return self._pack[file]
end

function GGE资源:删除资源包(file)
    self._pack[file] = nil
end

function GGE资源:添加路径(p)
    self._path[p] = true
end

function GGE资源:删除路径(p)
    self._path[p] = nil
end

function GGE资源:是否存在(path,...)
    if select("#", ...)>0 then
        path = path:format(...)
    end
    --绝对路径
    local file<close> = RWFromFile(path,"r")
    if file then
        return path
    end
    --搜索路径
    for k,_ in pairs(self._path) do
        local file<close> = RWFromFile(k.."/"..path,"r")
        if file then
            return k.."/"..path,k
        end
    end
    --搜索包
    for f,p in pairs(self._pack) do
        if p:是否存在(path) then
            return p,f
        end
    end
    return false
end

function GGE资源:读数据(path,...)
    if select("#", ...)>0 then
        path = path:format(...)
    end
    local r = self:是否存在(path)
    if r then
        if type(r)=='string' then
            return LoadFile(r)--SDL
        end
        return r:读数据(path)--pack
    end
    return nil
end

function GGE资源:取纹理(...)
    local data = self:读数据(...)
    if data then
        return require("SDL.纹理")(data,#data)
    end
end

function GGE资源:取精灵(...)
    local tex = self:取纹理(...)
    if tex then
        return require("SDL.精灵")(tex)
    end
end

function GGE资源:取图像(...)
    local data = self:读数据(...)
    if data then
        return require("SDL.图像")(data,#data)
    end
end

-- function GGE资源:取动画(file)
--     return require("SDL.纹理")(file)
-- end

function GGE资源:取音效(...)
    local data = self:读数据(...)
    if data then
        return require("SDL.音效")(data,#data)
    end
end

function GGE资源:取文字(...)
    local data = self:读数据(...)
    if data then
        return require("SDL.文字")(data,#data)
    end
end

--======================================================================================================
GGE资源包 = class("GGE资源包")

function GGE资源包:初始化(file,psd)
    local db = require("lib.sqlite3")(file,psd)
    if db then
        local r = db:取行数("select count(*) from sqlite_master where name='file';")
        if r==0 then
            db:执行[[
                CREATE TABLE "main"."file" (
                    "path"  TEXT,
                    "md5"  TEXT,
                    PRIMARY KEY ("path")
                );

                CREATE UNIQUE INDEX "main"."spath"
                ON "file" ("path" ASC);

                CREATE TABLE "main"."data" (
                    "type"  INTEGER DEFAULT 0,
                    "md5"  TEXT,
                    "time"  INTEGER,
                    "size"  INTEGER,
                    "dsize"  INTEGER,
                    "data"  BLOB,
                    PRIMARY KEY ("md5")
                );

                CREATE UNIQUE INDEX "main"."smd5"
                ON "data" ("md5" ASC);
            ]]
        end
        self._db = db
    else
        error("资源包打开失败。")
    end
end

function GGE资源包:是否存在(path)
    return self._db:取行数("select count(*) from file where path = '%s'; ", path)~=0
end

function GGE资源包:读数据(path)
    local t = self._db:查询一行("SELECT data FROM data WHERE md5 = (SELECT md5 FROM file WHERE path = '%s');",path)
    return t and t.data
end

function GGE资源包:写数据(path,data)

end
return GGE资源