--[[
    @Author       : baidwwy
    @Date         : 2021-02-11 11:49:09
    @LastEditTime : 2021-03-09 04:07:44
--]]

local _ENV = require("SDL")
local GGE矩形 = class"GGE矩形"
package.loaded['GGE.包围盒'] = GGE矩形
package.loaded['GGE包围盒'] = GGE矩形

function GGE矩形:GGE矩形(x,y,w,h)
    if ggetype(x)=='SDL_Rect' then
        self._r = x
    else
        self._r = CreateRect(x,y,w,h)
    end
end

function GGE矩形:__newindex(k,v)
    if k=='x' then
        self._r:SetRectXY(v)
    elseif k=='y' then
        self._r:SetRectXY(nil,v)
    elseif k=='w' then
        self._r:SetRectWH(v)
    elseif k=='h' then
        self._r:SetRectWH(nil,v)
    else
        rawset(self, k, v)
    end
end

function GGE矩形:__eq(t)
    local a,b = self._r,t._r
    return (a.x == b.x) and (a.y == b.y) and (a.w == b.w) and (a.h == b.h)
end

function GGE矩形:复制()
    return GGE矩形(self._r:GetRect())
end

function GGE矩形:显示(x,y)
    if self.r then
        引擎:置颜色(self.r,self.g,self.b)
    end
    if not y and ggetype(x) == 'GGE坐标' then
        x,y = x:unpack()
    end
    if x and y then
        local _x,_y = self._r:GetRectXY()
        self._r:SetRectXY(x+_x,y+_y)
        引擎:画矩形(self._r)
        self._r:SetRectXY(_x,_y)
    else
        引擎:画矩形(self._r)
    end
end

function GGE矩形:置颜色(r,g,b)
    self.r,self.g,self.b = r,g,b
    return self
end

function GGE矩形:置中心(x,y)
    self._hx = x
    self._hy = y
    return self
end

function GGE矩形:取中心()
    return self._hx,self._hy
end

function GGE矩形:置宽高(w,h)
    self._r:SetRectWH(w,h)
    return self
end

function GGE矩形:取宽高()
    return self._r:GetRectWH()
end

function GGE矩形:置坐标(x,y)
    self._r:SetRectXY(x,y)
    return self
end

function GGE矩形:取坐标()
    return self._r:GetRectXY()
end

function GGE矩形:清空()

end

function GGE矩形:检查点(x,y)
    if not y and ggetype(x) == 'GGE坐标' then
        x,y = x:unpack()
    end
    local _x,_y = self._r:GetRectXY()--self._r.x,self._r.y
    local _w,_h = self._r:GetRectWH()--self._r.w,self._r.h
    if self._hx then
        _x = _x - self._hx
    end
    if self._hy then
        _y = _y - self._hy
    end
    return (x>=_x) and (x<_x+_w) and (y>=_y) and (y<_y+_h)
end

function GGE矩形:检查(t)
    return self._r:HasIntersection(t._r)
end

function GGE矩形:取交集(t)
    local r = GGE矩形()
    if self._r:HasIntersection(t._r) then
        r._r = self._r:IntersectRect(t._r)
    end
    return r
end

function GGE矩形:取并集(t)
    local r = GGE矩形()
    r._r = self._r:UnionRect(t._r)
    return r
end
--EnclosePoints
--IntersectRectAndLine
function GGE矩形:取对象()
    return self._r
end

return GGE矩形